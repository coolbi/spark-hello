package com.project

import org.apache.spark.{SparkConf, SparkContext}

object Hello {
  def main(args: Array[String]): Unit = {
    println("main begin...............")
    // 设置Spark的序列化方式
    System.setProperty("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    // 初始化Spark
    val config = Map(
      "spark.cores" -> "local[*]"
    )
    val sparkConf = new SparkConf().setMaster(config("spark.cores")).setAppName("DataLoader")
    val sc = new SparkContext(sparkConf)

    val a = sc.parallelize(List(1, 2, 3, 3))
    val b = a.map(x => x + 1)

    //    textfileTest("C:/Users/think/Desktop/1.txt");

    val colors = Map("red" -> "#FF0000",
      "azure" -> "#F0FFFF",
      "peru" -> "#CD853F")

    val nums: Map[Int, Int] = Map()

    println("colors 中的键为 : " + colors.keys)
    println("colors 中的值为 : " + colors.values)
    println("检测 colors 是否为空 : " + colors.isEmpty)
    println("检测 nums 是否为空 : " + nums.isEmpty)
    println("main end...............")
  }
}
